<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;


trait ApiResponser
{
	/**
	 * Función de respuesta de éxito generalizada de la API
	 * 
	 * @param array|object $data
	 * @param int          $code
	 * @return Response
	 */
	private function successResponse($data, $code)
	{
		return response()->json($data, $code);
	}

	/**
	 * Función de respuesta de error generalizada de la API
	 * 
	 * @param array|object $data
	 * @param int          $code
	 * @return Response
	 */
	protected function errorResponse($message, $code)
	{
		return response()->json(['error' => $message, 'code' => $code], $code);
	}

	/**
	 * Función de respuesta de colección generalizada de la API
	 * 
	 * @param Illuminate\Database\Eloquent\Collection $collection
	 * @param int          							  $code
	 * @return Response
	 */
	protected function showAll(Collection $collection, $code = 200)
	{
		return $this->successResponse(['data' => $collection], $code);
	}

	/**
	 * Función de respuesta de instancia generalizada de la API
	 * 
	 * @param Illuminate\Database\Eloquent\Model $instance
	 * @param int          						 $code
	 * @return Response
	 */
	protected function showOne(Model $instance, $code = 200)
	{
		return $this->successResponse(['data' => $instance], $code);
	}
}